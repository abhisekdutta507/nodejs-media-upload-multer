## Check the Postman [collection](./Node.js multer.postman_collection.json).

### Node.js API Documentation

**UPLOAD multiple `media` API is documented below.**

```javascript
const formdata = new FormData();
formdata.append('media', fileInput.files[0], '/C:/Users/Laptop/Pictures/api 2.png');
formdata.append('media', fileInput.files[0], '/C:/Users/Laptop/Pictures/snippet 1.png');

const requestOptions = {
  method: 'POST',
  body: formdata,
  redirect: 'follow'
};

const baseURL = 'http://localhost:5000';

fetch(`${baseURL}/api/v1/uploadmedia`, requestOptions)
  .then(response => response.text())
  .then(result => console.log(result))
  .catch(error => console.log('error', error));
``` 
#RESPONSE

```json
{
    "uploaded": true,
    "fileUrls": [
        "http://localhost:5000/media/uploaded/nodeapis101-generated-media-1645904616677.png",
        "http://localhost:5000/media/uploaded/nodeapis101-generated-media-1645904616678.png"
    ]
}
```

Thanks for reading!!
