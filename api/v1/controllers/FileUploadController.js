const _ = require('../models/')._;

const FileUploadController = {

  create: async (req, res) => {
    let URLs = [];
    if(req.files.length) { URLs = _.map(req.files, f => { return (process.env.DEVELOPMENT ? `http://localhost:${process.env.PORT}/media/uploaded/` : 'https://nodeapis101.herokuapp.com/media/uploaded/') + f.filename; }); }
    else { return res.status(200).send({ uploaded: false, fileUrls: URLs }); }
    return res.status(200).send({ uploaded: true, fileUrls: URLs });
  },

};

module.exports = FileUploadController;