const file = {
  filterHTML: function(req, file, cb) {
    if (!file.originalname.match(/\.(html)$/)) {
      return cb(new Error('Only html files are allowed!'), false);
    }
    cb(null, true);
  },

  filterImage: function(req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif|mp4)$/)) {
      return cb(new Error('Only jpg, jpeg, png, gif & mp4 files are allowed!'), false);
    }
    cb(null, true);
  },

  getName: function(req, file, cb) {
    cb(
      null,
      'nodeapis101-generated-' + file.fieldname + '-' + Date.now() + file.originalname.slice(file.originalname.lastIndexOf('.'), file.originalname.length)
    );
  }
};

module.exports = file;