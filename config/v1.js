const express = require('express');
const multer = require('multer');
const Policies = require('../api/v1/policies');
const FileHelper = require('../api/v1/helpers/file');

let storage = multer.diskStorage({ destination: './public/media/uploaded/', filename: FileHelper.getName });
const UploadMedia = multer({ storage: storage, fileFilter: FileHelper.filterImage });

const FileUploadController = require('../api/v1/controllers/FileUploadController');

const api = express.Router();

/**
 * @description version 1 API routes
 */
const routes = () => {
  /**
   * @description Image upload
   */
  api.post('/uploadmedia', UploadMedia.array('media', 10), FileUploadController.create);

  return api;
};

module.exports = routes();