const env = {
  ao: [
    'http://localhost'
    , 'http://localhost:3000'
    , 'http://localhost:4000'
    , 'http://localhost:4001'
    , 'http://localhost:4002'
    , 'http://localhost:4003'
    , 'http://localhost:4004'
    , 'http://localhost:4005'
    , 'http://localhost:4200'
    , 'https://inhouseescrow.com'
  ]
};

module.exports = env;